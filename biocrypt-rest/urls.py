from django.contrib import admin
from django.conf.urls import include, url
from django.conf import settings
from django.conf.urls.static import static

from . import api_urls

urlpatterns = [
    url('admin/', admin.site.urls),
    url(r'^shares/', include('shares.urls')),
    url(r'^auth/', include('authentication.urls')),

    url(r'^api/', include(api_urls)),

]

admin.site.site_header = "BioCrypt Administration"
admin.site.site_title = "BioCrypt Admin Portal"
admin.site.index_title = "Welcome to BioCrypt Admin Portal"

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
