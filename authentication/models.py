from django.db import models
import hashlib


# Create your models here.
class User(models.Model):
    name = models.CharField(max_length=100)
    username = models.CharField(max_length=100)
    pin_hash = models.CharField(max_length=500)

    def createNewUser(self, name, username, pin):
        self.name = name
        self.username = username
        sha = hashlib.sha256()
        sha.update(str(pin).encode('utf-8'))
        self.pin_hash = sha.hexdigest()

    def __str__(self):
        return str(self.id) + '-' + str(self.username)
