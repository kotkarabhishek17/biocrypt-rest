from rest_framework import serializers

from shares.models import Share, UserShareMapping


class ShareSerializer(serializers.ModelSerializer):
    class Meta:
        model = Share
        fields = '__all__'


class UserShareMappingSerializer(serializers.ModelSerializer):
    shares = ShareSerializer(many=True)

    class Meta:
        model = UserShareMapping
        fields = ['user_id', 'fingerprint_id', 'shares']
