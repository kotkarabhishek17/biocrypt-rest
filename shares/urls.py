from django.conf.urls import url
from shares.views import delete_shares, delete_user_mappings, delete_users

app_name = 'shares'

urlpatterns = [
    url(r'^delete-shares/$', delete_shares, name='delete_shares'),
    url(r'^delete-user-mappings/$', delete_user_mappings, name='delete_user_mappings'),
    url(r'^delete-users/$', delete_users, name='delete_users'),
]
