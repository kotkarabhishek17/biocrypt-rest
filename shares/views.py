from rest_framework import generics, permissions, status
from rest_framework.renderers import JSONRenderer

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.response import Response

from authentication.models import User
from .models import Share, UserShareMapping
from .serializers import UserShareMappingSerializer


class ShareCreateView(generics.CreateAPIView):
    permission_classes = [
        permissions.AllowAny
    ]
    serializer_class = UserShareMappingSerializer

    def get_object(self):
        user_id = self.kwargs.get('user_id', None)
        fingerprint_id = self.kwargs.get('fingerprint_id', None)
        obj, created = UserShareMapping.objects.get_or_create(
            user_id=user_id,
            fingerprint_id=fingerprint_id
        )
        return obj

    def create(self, request, *args, **kwargs):
        share_number = request.data.get('share_number', None)
        share_data = request.data.get('share_data', None)

        share = Share.objects.create(share_number=share_number, share_data=share_data)

        user_share_mapping = self.get_object()
        user_share_mapping.shares.add(share)

        return Response({
            'status': status.HTTP_200_OK,
            'share_id': share.id
        })


class ShareListAPIView(generics.ListAPIView):
    renderer_classes = [JSONRenderer]
    permission_classes = [
        permissions.AllowAny
    ]
    serializer_class = UserShareMappingSerializer

    def get_queryset(self):
        user_id = self.kwargs.get('user_id')
        return UserShareMapping.objects.filter(user_id=user_id)


class ShareRetrieveView(generics.RetrieveAPIView):
    permission_classes = [
        permissions.AllowAny
    ]
    serializer_class = UserShareMappingSerializer

    def get_object(self):
        user_id = self.kwargs.get('user_id', None)
        fingerprint_id = self.kwargs.get('fingerprint_id', None)
        obj, created = UserShareMapping.objects.get_or_create(
            user_id=user_id,
            fingerprint_id=fingerprint_id
        )
        return obj


@csrf_exempt
def delete_shares(request):
    Share.objects.all().delete()
    return HttpResponse("")


@csrf_exempt
def delete_user_mappings(request):
    UserShareMapping.objects.all().delete()
    return HttpResponse("")


@csrf_exempt
def delete_users(request):
    User.objects.all().delete()
    return HttpResponse("")
