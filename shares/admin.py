from django.contrib import admin
from .models import Share, UserShareMapping


class ShareAdmin(admin.ModelAdmin):
    list_display = ['id', 'share_number']
    search_fields = ['id', 'share_data']


class ShareInline(admin.TabularInline):
    model = UserShareMapping.shares.through
    extra = 0
    verbose_name = "Share"
    verbose_name_plural = "Shares"

class UserShareMappingAdmin(admin.ModelAdmin):
    inlines = [ShareInline]
    list_display = ['user_id', 'fingerprint_id']
    search_fields = ['user_id']
    fields = ['user_id', 'fingerprint_id']


admin.site.register(Share, ShareAdmin)
admin.site.register(UserShareMapping, UserShareMappingAdmin)
